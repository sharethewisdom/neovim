if vim.g.sysinit_loaded == 1 then
  return
end
vim.g.sysinit_loaded = 1
require'plugins'
require'mapping'
require'options'
vim.cmd('filetype plugin indent on')
vim.cmd('syntax on')
vim.g.loaded_spellfile_plugin = 1

-- for creating hashed aliases in the sudoers file
vim.cmd('command! -nargs=1 Dgst !openssl dgst -binary -sha224 <args> | openssl base64')
