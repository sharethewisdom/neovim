vim.cmd('packadd packer.nvim')

return require('packer').startup(
function(use)
  use 'wbthomason/packer.nvim'
  use {
    'kyazdani42/nvim-web-devicons',
    config = function()
      require('nvim-web-devicons').setup{
        override = {
          zsh = {
            icon = "",
            color = "#428850",
            name = "Zsh"
          }
        };
        default = true;
      }
    end
  }
  use {
    'norcalli/nvim-colorizer.lua',
    config = function()
      require('colorizer').setup{
        sass = { css = true };
        css = { css = true };
        javascript = { css = true, names = false };
        html = { names = false };
      }
    end
  }
  use 'JoosepAlviste/nvim-ts-context-commentstring'
  use 'sainnhe/edge'
  use 'tpope/vim-commentary'        -- Tim Pope
  use 'tpope/vim-repeat'            -- Tim Pope
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
    config = function()
      require('nvim-treesitter.configs').setup{
        context_commentstring = { enable = true },
        ensure_installed = {
          "bash", "beancount", "bibtex",
          "c", "css", "go", "html",
          "javascript", "latex", "lua",
          "php", "python", "regex"
        },
        indent = { enable = true },
        highlight = { enable = true },
      };
    end
  }
  use {
    'steelsojka/pears.nvim',
    config = function() require('pears').setup() end
  }
  use {
    'neovim/nvim-lspconfig',
    config = function()
      require('lspconfig').cssls.setup{on_attach=require'completion'.on_attach}
      require('lspconfig').html.setup{on_attach=require'completion'.on_attach}
      require('lspconfig').jsonls.setup{on_attach=require'completion'.on_attach}
      require('lspconfig').pyls.setup{on_attach=require'completion'.on_attach}
      require('lspconfig').sumneko_lua.setup{
        on_attach=require'completion'.on_attach,
        cmd = {"usr", "bin", "lua-language-server"};
      }
      vim.cmd[[autocmd BufEnter * lua require'completion'.on_attach()]]
    end
  }
  use 'romainl/vim-cool'
  -- use 'tommcdo/vim-lion'            -- Tom McDonald
  use {
    'glepnir/galaxyline.nvim',
    branch = 'main',
    event = {'VimEnter *'},
    config = function() require('statusline') end
  }
  use 'nvim-lua/completion-nvim'
end
)
    -- use 'vijaymarupudi/nvim-fzf'
    -- use 'Iron-E/nvim-libmodal'
