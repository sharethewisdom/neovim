if vim.g.sysinit_loaded == 1 then
  return
end
vim.g.sysinit_loaded = 1
require('plugins')
require('options')
require('mapping')
