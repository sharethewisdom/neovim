o,g,w,b,fn,cmd,exec = vim.o,vim.g,vim.wo,vim.bo,vim.fn,vim.cmd,vim.api.nvim_command

local nmap = function (key, result)
  vim.api.nvim_set_keymap('n', key,  result, {
    noremap = true, silent = true
  })
end
local vmap = function (key, result)
  vim.api.nvim_set_keymap('v', key,  result, {
    noremap = true, silent = true
  })
end
local cmap = function (key, result)
  vim.api.nvim_set_keymap('v', key,  result, {
    noremap = true, silent = true, expr = true
  })
end
local imap = function (key, result, expression)
  expression = expression or false
  vim.api.nvim_set_keymap('i', key,  result, {
    noremap = true, silent = true, expr = expression
  })
end

local function check_back_space()
  local col = fn.col('.') - 1
  return col <= 0 or fn.getline('.'):sub(col, col):match('%s')
end

nmap('<C-b>', '<cmd>NvimTreeToggle<CR>')
nmap('<C-z>', '<cmd>NvimTreeFindFile<CR>')

vim.api.nvim_set_keymap('t', '<esc>', '<c-\\><c-n>', {
  noremap = true, silent = true
})

nmap('<space>', 'zxzjzo')
nmap('<leader><space>', 'zxzkzo[z')

nmap('<c-l>', '<cmd>nohlsearch<cr>')
nmap('<leader><space>', 'zxzkzo[z')
nmap('<left>',  '<cmd>tabp<cr>')
nmap('<right>', '<cmd>tabn<cr>')
nmap('<space>', 'zxzjzo')
nmap('gf', '<C-w>gf')
nmap('gF', '<C-w>gF')
-- I rather like Q to be a bit less accessible with gQ.
nmap('Q', '<nop>')
nmap('y', 'myy`y')
nmap('Y', 'y$')

vmap('<', '<gv') -- Reselect the visual block after indent
vmap('>', '>gv') -- Reselect the visual block after outdent

_G.mapping = {}
function _G.mapping.firstCharOrCol()
  local current_col = fn.virtcol('.')
  exec('normal _')
  local first_char = fn.virtcol('.')
  if (current_col == first_char) then
    fn.cursor('.',1)
  end
end

-- TODO: review logic
function _G.mapping.lastCharOrCol()
  local current_col = fn.virtcol('.')
  if w.wrap then
    exec('execute "normal <End>"')
  elseif (o.virtualedit == nil) then
    exec('execute "normal g$"')
  else
    exec('execute "normal $"')
  end
  local last_char = fn.virtcol('.')
  if (current_col == last_char) then
    if (w.textwidth ~= nil) then
    -- if (w.textwidth ~= nil and o.virtualedit ~= nil) then
      exec('execute "normal "'..w.textwidth..'|')
    end
  end
end

nmap('0', '<Cmd>call v:lua.mapping.firstCharOrCol()<CR>')
nmap('-', '<Cmd>call v:lua.mapping.lastCharOrCol()<CR>')
vmap('0', '<Cmd>call v:lua.mapping.firstCharOrCol()<CR>')
vmap('-', '<Cmd>call v:lua.mapping.lastCharOrCol()<CR>')

_G.completion = {}
function _G.completion.tab(reverse)
  if fn.pumvisible() > 0 then
    if reverse then return '<C-p>' else return '<C-n>' end
  end

  if check_back_space() then
    return '<TAB>'
  end
end

imap('<tab>',   'v:lua.completion.tab(false)')
imap('<S-tab>', 'v:lua.completion.tab(true)')
