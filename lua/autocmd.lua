vim.cmd[[augroup GuiCursor]]
  vim.cmd[[autocmd!]]
  vim.cmd[[autocmd VimEnter,VimResume *
    set guicursor=n-v-c:block
    ,i-ci-ve:ver25,r-cr:hor20,o:hor50
    ,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
    ,sm:block-blinkwait175-blinkoff150-blinkon175]]
  vim.cmd[[autocmd VimLeave,VimSuspend * set guicursor=a:ver30]]
  vim.cmd[[autocmd TextYankPost * silent! lua vim.highlight.on_yank
    {higroup="IncSearch", timeout=150}]]
vim.cmd[[augroup END]]
