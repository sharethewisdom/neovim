o = vim.o
g = vim.g
w = vim.wo
b = vim.bo
fn = vim.fn
cmd = vim.cmd
exec = vim.api.nvim_command

o.autoindent    = true
o.breakindent   = true
o.expandtab     = true
o.incsearch     = true
o.modeline      = true
o.showcmd       = true -- Show me what I'm typing
o.showmatch     = true -- Do not show matching brackets by flickering
o.splitright    = true
o.termguicolors = true -- True color support
o.undofile      = true -- Maintain undo history between sessions
o.wildmenu      = true
o.wrapscan      = true
o.writebackup   = true

w.wrap           = false
w.number         = true
w.relativenumber = true
w.list           = true

o.completeopt    = 'menuone,noselect'
o.breakindentopt = 'min:20,shift:0,sbr'
o.wildmode       = 'longest:full,full'
o.wildoptions    = 'pum,tagfile'
o.shortmess      = 'caOstTIF'
o.inccommand     = 'nosplit'
o.shada          = "'0,s100"
o.cedit          = '<c-f>'
o.commentstring  = '# %s'
o.virtualedit    = 'all'

o.undolevels    = 1000 -- Ensure we can undo...a lot
o.history       = 5000
o.cmdheight     = 3
o.tabpagemax    = 20
o.previewheight = 14
o.pumheight     = 14
o.tabstop       = 2
b.tabstop       = 2
o.shiftwidth    = 2
b.shiftwidth    = 2
o.softtabstop   = 2
b.softtabstop   = 2
o.sidescroll    = 6
o.sidescrolloff = 6

g.mapleader = ','
g.maplocalleader = '\\'

g.man_hardwrap=0
g.ft_man_folding_enable=1

g.edge_style = 'aura'
g.edge_enable_italic = 1
g.edge_disable_italic_comment = 1
g.edge_transparent_background = 1
cmd('colorscheme edge')

-- set foldmethod=expr
-- set foldexpr=nvim_treesitter#foldexpr()

local ignores = {
  '*.o',
  '*.obj,*~',
  '*.git*',
  '*.meteor*',
  '*vim/backups*',
  '*sass-cache*',
  '*mypy_cache*',
  '*__pycache__*',
  '*cache*',
  '*logs*',
  '*node_modules*',
  '**/node_modules/**',
  '*DS_Store*',
  '*.gem',
  'log/**',
  'tmp/**',
}
o.wildignore = table.concat(ignores, ',')
local skip = {
  '/tmp/*', '*.gpg',
  '/mnt/*', '/etc/*', '/proc/*',
  '{COMMIT_EDIT,TAG_EDIT,MERGE_,}MSG'
}
o.backupskip = table.concat(skip, ',')
local suffixes = {
  '.bak', '~', '.o', '.h',
  '.info', '.swp', '.obj',
  '.pdf', '.xml', '.csv',
  '.pdf', '.xml', '.csv',
  '.git', '.tar', '.tgz',
  '.xz', '.zst', '.deb',
  '.cpio', '.dll', '.md5',
  '.log', '.7z', '.so',
  '.pkg', '.jpg', '.jpeg',
  '.mp3', '.wav', '.gif',
  '.webm', '.pyc', '.pyg'
}
o.suffixes = table.concat(suffixes, ',')

-- for s:dir in ["view","swap","back","undo"]
--   if !isdirectory(expand(g:xch."/".s:dir))
--     call mkdir(expand(g:xch."/".s:dir), 'p', 0740)
--   endif
-- endfor
-- let &viewdir   = expand(g:xch."/view")
-- let &directory = expand(g:xch."/swap")."//,/tmp//"
-- let &backupdir = expand(g:xch."/back")."//,/tmp//"
-- let &undodir   = expand(g:xch."/undo")."//,/tmp//"

for _, d in ipairs({"view","swap","back","undo","sess"}) do
  local dir = fn.stdpath('config')..'/'..d
  if fn.isdirectory(dir) == 0 then
    fn.mkdir(dir, 'p', 0755)
  end
end

o.viewdir   = fn.stdpath('config')..'/view' -- Store files for :mkview
o.backupdir = fn.stdpath('config')..'/back' -- Use backup files
o.directory = fn.stdpath('config')..'/swap' -- Use Swap files
o.undodir   = fn.stdpath('config')..'/undo' -- Set the undo directory
sessiondir  = fn.stdpath('config')..'/sess' -- Set the session directory

-- function! init#darkorlight(file) abort
--   if getftype(expand(a:file)) != 'link'
--     return
--   endif
--   if resolve(glob(expand(a:file))) =~ 'dark.conf$'
--     set background=dark
--   else
--     set background=light
--   endif
--   redraw
-- endfunction
if fn.getftype('$HOME/.config/termite/config') == 'link' then
  if string.match(fn.resolve(glob('$HOME/.config/termite/config')),'dark.conf') then
    vim.o.background='dark'
  else
    vim.o.background='light'
  end
end
