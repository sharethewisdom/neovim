" vim: ft=vim ts=2 sw=2 :

function! s:firstCharOrCol() abort
  let current_col = virtcol('.')
  normal _
  let first_char = virtcol('.')
  if current_col == first_char
    call cursor('.',1)
  endif
endfunction

function! s:lastCharOrCol() abort
  let current_col = virtcol('.')
  if &wrap > 0
    execute "normal \<End>"
  else
    execute "normal g$"
  endif
  let last_char = virtcol('.')
  if current_col == last_char
    if &textwidth > 0
      execute "normal " . &textwidth . "|"
    endif
  endif
endfunction

nnoremap <silent> 0 <Cmd>call <SID>firstCharOrCol()<CR>
nnoremap <silent> - <Cmd>call <SID>lastCharOrCol()<CR>
vnoremap <silent> 0 <Cmd>call <SID>firstCharOrCol()<CR>
vnoremap <silent> - <Cmd>call <SID>lastCharOrCol()<CR>

autocmd Filetype help\|man autocmd! BufWinEnter <buffer> :silent vertical resize 80<cr>
autocmd Filetype help\|man nnoremap <silent><buffer> <cr> 
autocmd Filetype help\|man nnoremap <silent><buffer> <bs> <c-o>
autocmd Filetype help\|man nnoremap <silent><buffer> q <Cmd>bwipeout<cr>

cnoreabbrev h vert help
cnoreabbrev m vert Man
cnoreabbrev 000 >/dev/null 2>&1 &\|
inoreabbrev todo: TODO:

if !empty(globpath(&rtp, 'plugin/man.vim')) && !exists("g:loaded_man")
  runtime plugin/man.vim
endif

if !empty(globpath(&rtp, 'plugin/fzf.vim')) && !exists("g:loaded_fzf")
  let g:fzf_layout = { 'right': '60%' }
  runtime plugin/fzf.vim
endif
