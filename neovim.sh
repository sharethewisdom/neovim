export MANPAGER="nvim +Man!"
export MANWIDTH=999
export ASAN_OPTIONS="detect_leaks=0:log_path=${XDG_DATA_HOME}/nvim/asan-`date +%F`.log"
export NVIM_LOG_FILE="${XDG_DATA_HOME}/nvim/asan-`date +%F`.log"
export NVIM_PYTHON_LOG_FILE="${XDG_DATA_HOME}/nvim/python-`date +%F`.log"
export UBSAN_OPTIONS=print_stacktrace=1
export SHELL=/bin/zsh
export ZDOTDIR=${ZDOTDIR:=$XDG_CONFIG_HOME/zsh}
export HELPDIR=/etc/zsh/help
export KEYTIMEOUT=1
export ZLE_RPROMPT_INDENT=1
export ZBEEP='\e[?5h\e[?5l'
export WORDCHARS='*?_-./[]~=&;!#$%^(){}<>'
export FZF_DEFAULT_COMMAND="find . -maxdepth 5 -path .git -prune -o -type f | fzf -1 -0 -e --reverse --header='Search file'"

git clone https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim
#git clone https://github.com/neovim/nvim-lspconfig ~/.local/share/nvim/site/pack/packer/start/nvim-lspconfig
