augroup filetypedetect
autocmd BufNewFile,BufRead *.conf{,ig}         if empty(&ft) | setl filetype=config | endif
augroup END
